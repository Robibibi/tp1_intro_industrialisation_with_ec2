from flask import Flask
import boto3


def insert_user_into_dynamo(user):
    dynamodb = boto3.resource("dynamodb")
    table = dynamodb.Table("tp-table")
    table.put_item(Item={"noteId": user["id"]})


app = Flask(__name__)


@app.route("/")
def hello():
    return "Hello World!"


@app.route("/goodBye")
def goodbye():
    return "Good Bye World!"


@app.route("/add")
def add(id):
    insert_user_into_dynamo({"id": id})
    return "added " + str(id)


print("starting!!")
if __name__ == "__main__":
    print("starting..")
    app.run(host="0.0.0.0")
